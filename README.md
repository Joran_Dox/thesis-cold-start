# README

This project contains the source code used for my thesis: "Solving the Cold Start problem with Local Collective Embeddings"

If you've arrived here through Appendix A in that thesis, the file you're looking for is "Thesis running example.tex" for the LaTeX version, or "Thesis running example.ipynb" for the Jupyter version. In the Jupyter version you can make changes to the code as you see fit and run it yourself using Jupyter Python (the easiest way to get this is installing [Anaconda](https://www.continuum.io/downloads), which comes with all the other dependant packages I used).

Apart from that, to run the movielens examples near the end of the file you'll want to download the correct dataset from their [site](https://grouplens.org/datasets/movielens/) and unzip it into the same directory as the .ipynb file, or change the file paths manually to accomodate.

The 1LCE directory contains the original matlab code provided by the authors of the LCE paper (see thesis bibliography), and the converted python files, along with some temporary files, snippets, etc.