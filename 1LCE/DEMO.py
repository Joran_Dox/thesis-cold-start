#!/usr/bin/env python3

import numpy as np
import scipy.io as sio
from scipy.sparse import coo_matrix
import math
import time
import LCE
from NDCG import NDCG

#print("you forgot to remove the quit line")
#exit()

# TODO change to command line arguments (?)
# "real" variables
# %age of the data that should be training data when splitting in train / test set
trainpercentage = 80
# number of "topics"
k = 500
# some variables for LCE
alpha = 0.5
lamb = 0.5
beta = 0.05
# amount of iterations: maximum maxiter,
maxiter = 500
# or until error is less than epsilon
# original epsilon = 0.001
epsilon = 1.

# debug variables
# whether to reload the variable from file 
# shouldn't be necessary if naming scheme is unique and var-gen is correct
reloadvars = False
# use a smaller part of the data for faster dev cycles
usepercentage = 100

# input variables
tags_input = '../data/ml-20m/genome-scores.csv'
ratings_input = '../data/ml-20m/ratings.csv'
# whether to use pretrained data
# no: 
#loadMML = False
# yes:
#loadMML = "ItemAverage"
#loadMML = "BiasedMatrixFactorization"
loadMML = "SVDPlusPlus"
#loadMML = "SigmoidSVDPlusPlus"


MML_ratings_input = '../2AtFM/prediction_' + str(loadMML) + '.txt'
FilenemaPre = "./data/"
FilenameExt = ".".join([str(usepercentage),str(loadMML),'npy'])

# managing verbosity
# these should print only if verbose is True, idem with debug and timing
debug =False
verbose = True
timing = True
vprint = print if verbose else lambda *a, **k: None
dprint = print if debug else lambda *a, **k: None
tprint = print if timing else lambda *a, **k: None


def makefilename(varname:str):
	return FilenemaPre + varname + FilenameExt

# almost matlab-style tic/toc
timings = []
def tic():
	timings.append(time.time())

def toc(arg):
	tprint(time.time() - timings.pop(), "s", arg)

# function to replace internal IDs with the external ones and vice versa
def replaceIDs(inlist, idlist):
	return [idlist[x] for x in inlist]


if __name__ == "__main__":
	# 1 load data:
	# 	1.1 transform data:
	# 		data => Xs = Items x Features
	# 		data => Xu = Items x Users
	# 	1.2 (optional) transform data some more:
	# 		- tfidf if the features were word frequencies
	# 		- turn movie ratings into binary data?
	# 	1.3 (optional) split data into train and test set


	#
	#
	# check out scipy io matlab file loading
	# https://docs.scipy.org/doc/scipy/reference/generated/scipy.io.loadmat.html#scipy.io.loadmat
	#

	#

	tstart = time.time()

	if not reloadvars:
		try:
			tic()
			vprint('Trying to load variables from saved states')

			# add things to load here, don't forget to save them too!
			Xs = np.load(makefilename('Xs'))
			Xu = np.load(makefilename('Xu'))

			ItemEnumToItemIDs = np.load(makefilename('ItemEnumToItemIDs'))
			ItemIDsToItemEnum = np.load(makefilename('ItemIDsToItemEnum'))

			UniqueItems = np.load(makefilename('UniqueItems'))
			UniqueTags  = np.load(makefilename('UniqueTags'))
			UniqueUsers = np.load(makefilename('UniqueUsers'))

			if timing:
				toc('Variables loaded successfully!')
			else:
				vprint('Variables loaded successfully!')

		except FileNotFoundError as e:
			vprint(e)
			vprint("Couldn't load the variables, generating them from source...")
			reloadvars = True

	# not an else because reloadvars can be set to true if the loading fails
	if reloadvars:
		'''
		Read the data here, either from the movielens data 
		or the pre-aggregated mymedialite version
		'''

		import load_ml_20m
		vprint("Reloading vars from original files")
		tic()
		rowXs, colXs, dataXs = load_ml_20m.loadXs(tags_input)
		toc("read Xs from " + tags_input)

		tic()
		if loadMML:
			import load_MML_out
			rowXu, colXu, dataXu = load_MML_out.loadXu(MML_ratings_input)
			toc("read Xu from " + MML_ratings_input)
		else:
			rowXu, colXu, dataXu = load_ml_20m.loadXu(ratings_input)
			toc("read Xu from " + ratings_input)

	tdata = time.time()
	if reloadvars:

		tprint("Total data reading time: ", tdata-tstart)

		tic()

		# these will come in handy later
		UniqueUsers = np.array(sorted(set(rowXu)))
		UniqueTags =  np.array(sorted(set(colXs)))
		UniqueItems = np.array(sorted(set(rowXs).intersection(set(colXu))))

		toc("Unique")

		tic()

		# this is to decrease the size we're working with to roughly usepercentage%
		delmask = []
		if usepercentage != 100:
			usepercentage -= 1
			for index in range(len(UniqueItems)):
				if (index % 100) > usepercentage:
					delmask.append(index)
			usepercentage += 1
		UniqueItems = np.delete(UniqueItems, delmask)


		# users and tags are already a gapless list starting from index 1
		# items is not, and should be replaced by its enumerated ids
		ItemEnumToItemIDs = {key: value for key,value in enumerate(UniqueItems)}
		ItemIDsToItemEnum = {value: key for key,value in enumerate(UniqueItems)}
		

		toc("enum")

		tic()
		# check some things
		if (debug):
			for x,y in ItemIDsToItemEnum.items():
				if x not in UniqueItems:
					dprint("Not in uniq:")
					dprint(y)
				if ItemEnumToItemIDs[y] != x:
					dprint(x)
					dprint(y)
					dprint(ItemIDsToItemEnum[x])
					dprint(ItemEnumToItemIDs[y])

			count = 0
			for x in UniqueItems:
				if x not in ItemIDsToItemEnum:
					count += 1
			dprint("count outside:")
			dprint(count)

		# also handy for later
		UniqueItemsEnum = replaceIDs(UniqueItems, ItemIDsToItemEnum)

		delmask = []
		for index, x in enumerate(rowXs):
			if x not in UniqueItems:
				delmask.append(index)
		# maybe just update in-place?
		del_rowXs  = np.delete(rowXs, delmask)
		del_colXs  = np.delete(colXs, delmask)
		del_dataXs = np.delete(dataXs, delmask)


		delmask = []
		for index, x in enumerate(colXu):
			if x not in UniqueItems:
				delmask.append(index)


		# maybe just update in-place?
		del_rowXu  = np.delete(rowXu, delmask)
		del_colXu  = np.delete(colXu, delmask)
		del_dataXu = np.delete(dataXu, delmask)


		toc("deletions")

		tic()

		# Xs = Items x Attr => dataXs, (rowXs, colXs)
		coo_Xs = coo_matrix( (
			del_dataXs, 
			( replaceIDs(del_rowXs, ItemIDsToItemEnum), del_colXs )
		) )
		# Xu = Items x Users => dataXu, (colXu, rowXu)
		coo_Xu = coo_matrix( (
			del_dataXu, 
			( replaceIDs(del_colXu, ItemIDsToItemEnum), del_rowXu)
		) )

		toc("Xs & Xu generated")


		tic()
		# asarray: when loading these matrices they're loaded as 2d arrays anyway
		Xs = np.asarray(coo_Xs.todense())
		Xu = np.asarray(coo_Xu.todense())
		toc("Xs & Xu todense")
		
		# add things to save here, don't forget to load them too!
		tic()
		np.save(makefilename('Xs'), Xs)
		np.save(makefilename('Xu'), Xu)
		np.save(makefilename('UniqueItems'), UniqueItems)
		np.save(makefilename('UniqueTags'), UniqueTags)
		np.save(makefilename('UniqueUsers'), UniqueUsers)
		np.save(makefilename('ItemEnumToItemIDs'), ItemEnumToItemIDs)
		np.save(makefilename('ItemIDsToItemEnum'), ItemIDsToItemEnum)
		toc('Saved variables')
		if not timing:
			vprint('Saved variables')


	tvars = time.time()
	tprint("Variables ready", tvars - tdata)
	
	tic()
	Nd = len(UniqueItems) #  10370 # unique movieIds both tagged and rated
	Nw = len(UniqueTags)  #   1128 # unique tags
	Na = len(UniqueUsers) # 138493 # unique users

	traintestsplit = int(round(Nd * trainpercentage / 100))
	train_split = range(0,traintestsplit - 1)
	dprint(train_split)
	test_split = range(int(traintestsplit), Nd - 1)
	dprint(test_split)
	#TODO: from here
	#vocab = 1:Nw; %
	#vocab = range(Nw) # 0:Nw-1

	dprint(np.shape(Xs)) # (10370, 1129)
	dprint(np.shape(Xu)) # (10370, 138494)

	#Xu_train = Xu(train_split, :);      % 8296 x 138493 
	Xu_train = Xu[train_split, ]
	dprint(np.shape(Xu_train))
	#Xs_train = counts(:, train_split)'; % 8296 x   1128 (Nw)
	Xs_train = Xs[train_split,]
	dprint(np.shape(Xs_train))
	#Xs_train = Xs_train(:, vocab);      % 8296 x   1128 (Nw)
	#Xs_train = Xs_train[:,vocab]
	#dprint(np.shape(Xs_train))

	#Xu_test = Xu(test_split, :);      % 2074 x 138493
	Xu_test = Xu[test_split, ]
	dprint(np.shape(Xu_test))
	#Xs_test = counts(:, test_split)'; % 2074 x   1128 (Nw)
	Xs_test = Xs[test_split,]
	dprint(np.shape(Xs_test))
	#Xs_test = Xs_test(:, vocab);      % 2074 x   1128 (Nw)
	#Xs_test = Xs_test[:,vocab];
	#dprint(np.shape(Xs_test))

	toc("Train/test split done")
	tic()
	np.save(makefilename('Xs_train'), Xs_train)
	np.save(makefilename('Xs_test' ), Xs_test )
	np.save(makefilename('Xu_train'), Xu_train)
	np.save(makefilename('Xu_test' ), Xu_test )
	toc("and saved")


	tprep = time.time()
	tprint("Total data preprocessing time: ", tprep-tvars)


	# 2 LCE:
	# Xs ~= W (Items x Topics) * Hs (Topics x Features)
	# Xu ~= W (Items x Topics) * Hu (Topics x Users)

	#A = construct_A(Xs_train, 1, true);
	tic()
	A = LCE.construct_A(Xs_train, 1, True)
	toc('construct A')

	#[~, Hs, Hu, ObjHistory] = LCE(
	#		Xs_train, L2_norm_row(Xu_train), A, k,
	#		alpha, beta, lambda, epsilon, maxiter, verbose);
	tic()
	W, Hs, Hu = LCE.LCE(
		Xs_train, LCE.L2_norm_row(Xu_train), A, k,
		alpha, beta, lamb, epsilon, maxiter, verbose)
	toc("LCE")

	tic()
	np.save(makefilename('W' ), W )
	np.save(makefilename('Hs'), Hs)
	np.save(makefilename('Hu'), Hu)
	toc("saved LCE results")

	# 3 (optional) test and score:
	# - NDCG
	tic()
	
	#Hs = np.load('Hs' + FilenameExt)
	#Hu = np.load('Hu' + FilenameExt)
	tic()
	#W_test = Xs_test / Hs;
	dprint("Xs_test shape", np.shape(Xs_test))
	dprint("Hs shape", np.shape(Hs))
	W_test = np.transpose(
		np.linalg.lstsq(
			np.transpose(Hs),
			np.transpose(Xs_test)
		)[0]
	).reshape(np.shape(Xs_test)[0],k)
	dprint("W_test", W_test)
	dprint("W_test shape", np.shape(W_test))
	
	#W_test(W_test < 0) = 0;
	W_test = np.array([ x if x >= 0 else 0 for x in W_test.flatten() ]).reshape(np.shape(W_test))
	dprint("W_test shape after", np.shape(W_test))

	#LCE_ranking = W_test*Hu; 
	LCE_ranking = np.matmul(W_test,Hu) # should be "equal to" Xu_test
	np.save(makefilename('W_test'     ), W_test     )
	np.save(makefilename('LCE_ranking'), LCE_ranking)

	toc("made and saved LCE_ranking (and W_test)")

	# we saved the rankings so you can redo them later with this line
	LCE_res = NDCG(LCE_ranking, Xu_test)
	print("NDCG:", LCE_res)
	toc("NDCG")
	