import numpy as np
import math


"""
function [ret] = recommend_users(Items, Hs, Hu, topk)
    % expected input like :
    % users = recommend_users(Xs_test(1,:), Hs, Hu, 5)
    % this should definitely work for a vector, might also work for 
    % a matrix as input
    
    % transform words into topics
    temp = (Items / Hs); 
    temp(temp < 0) = 0;
    
    % transform topics into users
    temp2 = temp * Hu; 
    
    % sort with indices
    [Y, I] = sort(temp2, 'descend'); 
    
    % return top k
    ret = [Y(:, 1:topk); I(:, 1:topk)]; 
    
end
"""
def recommend_users(Items, Hs, Hu, topk):
	#temp = (Items / Hs); 
	temp = np.transpose(
		np.linalg.lstsq(
			np.transpose(Hs),
			np.transpose(
				Items.reshape(
					1,
					np.shape(Items)[0]
				)
			)
		)[0]
	)

	#temp(temp < 0) = 0
	temp = np.array(
		[ x if x >= 0 else 0 for x in temp.flatten() ]
	).reshape(np.shape(temp))

	#temp2 = temp * Hu; 
	temp2 = np.matmul(temp, Hu)

	# there might be a more efficient way than sorting twice
	#[Y, I] = sort(temp2, 'descend');
	Y = np.sort(temp2)
	Y[:] = Y[:,::-1]
	I = np.argsort(temp2)[:,::-1]

	#ret = [Y(:, 1:topk); I(:, 1:topk)]; 
	ret = np.zeros((np.shape(Y)[0],topk))
	ret[0:np.shape(Y)[0], :] = Y[:,:topk] # populate ret with the scores
	ret[np.shape(Y)[0]:, :] = I[:,:topk] # populate ret with the indexes = ids


	return ret
