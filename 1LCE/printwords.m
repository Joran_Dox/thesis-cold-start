function [res] = printwords(x, k)

% print the top k tfidf scoring words of the given paper-column

% input is most likely a column from counts for words, 
% or a row for papers
% e.g. printwords(counts(:,1)) gives the first column
% printwords(vocab, k) gives all words

% alternatively: Xs_train and Xs_test are tfidf scores for the counts
% matrix and the correct slices of those should work too for words
% likewise for papers you can use Xu (I think)

%load('data/nips12raw_str602.mat', 'ptitles'); % the papers
load('data/nips12raw_str602.mat', 'wl'); % the wordlist

% assuming x is a 1-dimensional sparse matrix:

[~, i] = sort(x);


endindex = min(length(i), k);

res = cell(endindex,1);


for l = 1:endindex
    res(l) = wl(i(l)); % if you want the words given a paper
    %res(l) = ptitles(i(l)); % if you want the papers given a word
end

end