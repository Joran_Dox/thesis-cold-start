import numpy as np

def loadXu(ratings_input):
	with open(ratings_input) as ratings_in:

		# rowXu  = user
		# colXu  = item
		# dataXu = score
		rowXu, colXu, dataXu  = np.loadtxt(ratings_in, delimiter='	', skiprows=0, usecols=(0,1,2)
			,unpack=True)
		rowXu = rowXu.astype(int)
		colXu = colXu.astype(int)
		#data is a float which is the default so nothing needs to be done
	return rowXu, colXu, dataXu
