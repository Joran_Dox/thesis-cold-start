function [ret] = recommend_users(Items, Hs, Hu, topk)
    % expected input like :
    % users = recommend_users(Xs_test(1,:), Hs, Hu, 5)
    % this should definitely work for a vector, might also work for 
    % a matrix as input
    
    % transform words into topics
    temp = (Items / Hs); 
    temp(temp < 0) = 0;
    
    % transform topics into users
    temp2 = temp * Hu; 
    
    % sort with indices
    [Y, I] = sort(temp2, 'descend'); 
    
    % return top k
    ret = [Y(:, 1:topk); I(:, 1:topk)]; 
    
end