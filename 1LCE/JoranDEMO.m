%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% edit of DEMO.m by Joran
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% save data? (comment this line out or set to 0 for no, 1 for yes)
if (not(exist('autosave', 'var')))
    autosave = 1;
end
%% partial usage of data? (for faster development mostly)

% takes partial % of the data and uses that as input
%if (not (exist('partial', 'var')))
    % not used yet
%    partial = 20;
%end

% choose a percentage train/test split:
if (not (exist('trainpercentage', 'var')))
    trainpercentage = 80;
end

%% Loading the data & Transforming the data in a suitable matrix form
% 

if (not(exist('reload', 'var')) || reload == 1)
    fprintf('Reading data\n');
    tic
    
    % tags = table(movieID, tagID, relevance)
    tags = readtable('../data/ml-20m/genome-scores.csv');
    tags_movies = unique(tags(:, 1));
    tags_words = unique(tags(:, 2));
    
    % ratings = table(userID, movieID, rating, timestamp)
    ratings = readtable('../data/ml-20m/ratings.csv');
    ratings_users = unique(ratings(:,1));
    ratings_movies = unique(ratings(:,2));
    
    
    % find all movies with ratings AND tags
    movies_intersect = intersect(tags_movies, ratings_movies);
    
    % make an indexed table of those movies
    testtab = array2table((1:size(movies_intersect, 1))', ...
        'VariableNames',{'M_ID'});
    idtrans = [testtab movies_intersect];
    
    % augment the older tables with indexes, removing rows without indexes
    tags_augmented = innerjoin(tags, idtrans);
    ratings_augmented = innerjoin(ratings, idtrans);
    
    % do the same for users and tags?
    % nope: users and tags are already a gapless list
    
    % counts = 1128x131170 (tags x movies) sparse double
    counts = sparse(tags_augmented.tagId, tags_augmented.M_ID, tags_augmented.relevance);
    % Xu = 138493x131262 sparse double
    Xu = sparse(ratings_augmented.M_ID, ratings_augmented.userId, ratings_augmented.rating);
    clear tags; % not needed anymore
    clear ratings; % not needed anymore
    
    % INTERSECT(A,B) -> item ids -> change item ids to their id in 
    % the resultant matrix

    % don't read this data in multiple times, it'll be the same each time
    reload = 0;
    % no tfidf on this data
    do_tfidf = 0;
    toc
else
    fprintf('Skipping reading data because reload == 0\n')
end
%% Setting some vars
fprintf('Setting vars\n');
Nd = size(Xu,     1); %  10370 # unique movieIds both tagged and rated
Nw = size(counts, 1); %   1128 # unique tags
Na = size(Xu,     2); % 138493 # unique users


%% Setting some more vars
k = 500;
alpha = 0.5;
lambda = 0.5;
epsilon = 10.0;
maxiter = 500;
verbose = true;
beta = 0.05;
%% Do the train/test split

% examples from DEMO:
%train_time = find(sum(T(:,1:12),2)); % 1:1590, docIDs before year 13
%test_time  = find(sum(T(:,13),2)); % 1590:1740, docIDs since year 13


traintestsplit = round(Nd*trainpercentage/100);
train_split = 1:traintestsplit;
test_split = (traintestsplit+1):Nd;

vocab = 1:Nw; % ~ a single, filled row from counts?

Xu_train = Xu(train_split, :);      % 8296 x 138493 
% NOTE THE TRANSPOSE BELOW       V
Xs_train = counts(:, train_split)'; % 8296 x   1128 (Nw)
% next line does nothing?
Xs_train = Xs_train(:, vocab);      % 8296 x   1128 (Nw)

Xu_test = Xu(test_split, :);      % 2074 x 138493
% NOTE THE TRANSPOSE BELOW     V
Xs_test = counts(:, test_split)'; % 2074 x   1128 (Nw)
% next line does nothing?
Xs_test = Xs_test(:, vocab);      % 2074 x   1128 (Nw)

%% Preprocessing the data
fprintf('Preprocessing\n');
train_authors = (sum(Xu_train) > 0); % sparse bool: (1,1) to (1,2037), mostly 1
Xu_train = Xu_train(:, train_authors);
Xu_test = Xu_test(:, train_authors);

%% tfidf is in a separate file
% geen tfidf op de movielens tag data, die hebben al een 
% vergelijkbare modificatie ondergaan
%
% term frequency–inverse document frequency
% = #term / #docs the term appears in
% actually tf-idf = tf * log(|D| / n_occurences) in this case for
% normalization

if ( (exist('do_tfidf', 'var')) && do_tfidf == 1 )
    fprintf('tfidf')
    [Xs_train, Xs_test] = tfidf(Xs_train, Xs_test); 
else
    fprintf('skipping tfidf')
end




%% constructing the adjacency matrix
fprintf('Adjacency matrix\n');
A = construct_A(Xs_train, 1, true);

%% Running the LCE 
fprintf('LCE\n');
tic % timer start
[~, Hs, Hu, ObjHistory] = LCE(Xs_train, L2_norm_row(Xu_train), A, k, alpha, beta, lambda, epsilon, maxiter, verbose);
% ^ ~ instead of W bc you don't need W I guess ^
% ^^ also what's with the L2_norm_row? ^^
toc % timer end
figure
plot(ObjHistory)

%% Predictions
fprintf('Predictions\n');
size(Xs_test)
size(Hs)
W_test = Xs_test / Hs;
size(W_test)
W_test(W_test < 0) = 0;
LCE_ranking = W_test*Hu; %
LCE_res = NDCG(LCE_ranking, Xu_test); % 
fprintf('NDCG: %d\n', LCE_res);
LCE_res = NDCG_wiki(LCE_ranking, Xu_test); % 
fprintf('NDCG_wiki: %d\n', LCE_res);



%% save
if exist('autosave', 'var') && autosave == 1
    fprintf('Saving... \n');
    save JoranAutoSave -v7.3
end

%% done
fprintf('Done!\n');













