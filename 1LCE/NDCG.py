import numpy as np
import math

# function to calculate NDCG from two matrices with the same dimensions
def NDCG(P,Y):
	"""
	function [result] = NDCG(P,Y)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% P is the ranking provided
% Y is the binary matrix annotating the labels
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	"""

	if np.shape(P) != np.shape(Y):
		print(np.shape(P))
		print(np.shape(Y))
		print("matrix dimensions must be the same!")

	#[n, m] = size(P);
	n,m = np.shape(P)

	#num_rel = sum(Y,2);
	num_rel = Y.sum(1)
	
	#[~, idx] = sort(P, 2, 'descend');
	# comparable sort = 
	# B = np.sort(A, axis=1)
	# B[:] = B[:,::-1]
	# but we need the indexes only
	idx = np.argsort(P)[:,::-1]
	

	#result = 0;
	result = 0
	#denominator = [1 1./log2(2:m)];
	denominator = [x if x ==1 else (1/math.log(x,2)) for x in range(1,m+1)]
	#denominator = [(1/math.log(x,2)) for x in range(2,m+2)]
	

	#for i=1:n,
	for i in range(n): # 0-> n-1
		#	DCG = sum(Y(i,idx(i,:)) .* denominator);
		DCG = sum(Y[i,idx[i]] * denominator)
		#	IDCG = sum([1 1./log2(2:num_rel(i))]);
		IDCG = sum([x if x ==1 else (1/math.log(x,2)) for x in range(1,math.ceil(num_rel[i])+1)])
		#IDCG = sum([(1/math.log(x,2)) for x in range(1,num_rel[i]+1)])
		#	result = result + ((DCG / IDCG) / n);
		result += DCG / IDCG
	#end
	result /= n
	return result

if __name__ == "__main__":
	import sys
	if len(sys.argv) != 3:
		print("please provide (only) LCE_ranking and Xu_test file locations")
		sys.exit(-1)
	LCE_ranking = np.load(sys.argv[1])
	Xu_test = np.load(sys.argv[2])
	print("NDCG:", NDCG(LCE_ranking, Xu_test))