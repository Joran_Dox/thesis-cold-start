import numpy as np

def loadXs(tags_input):
	with open(tags_input) as tags_in:

		# rowXs  = item
		# colXs  = tag
		# dataXs = score
		rowXs, colXs, dataXs = np.loadtxt(tags_in, delimiter=',', skiprows=1, usecols=(0,1,2)
			,unpack=True)
		rowXs = rowXs.astype(int)
		colXs = colXs.astype(int)
		#data is a float which is the default so nothing needs to be done
	return rowXs, colXs, dataXs

def loadXu(ratings_input):
	with open(ratings_input) as ratings_in:

		# rowXu  = user
		# colXu  = item
		# dataXu = score
		rowXu, colXu, dataXu  = np.loadtxt(ratings_in, delimiter=',', skiprows=1, usecols=(0,1,2)
			,unpack=True)
		rowXu = rowXu.astype(int)
		colXu = colXu.astype(int)
		#data is a float which is the default so nothing needs to be done
	return rowXu, colXu, dataXu
