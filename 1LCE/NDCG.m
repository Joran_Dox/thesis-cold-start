function [result] = NDCG(P,Y)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% P is the ranking provided
% Y is the binary matrix annotating the labels
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[n, m] = size(P);
num_rel = sum(Y,2);
[~, idx] = sort(P, 2, 'descend');
result = 0;
denominator = [1 1./log2(2:m)];
% according to wikipedia, it should be:
%denominator = [1./log2(2:m+1)];
% mistake, fraud, or other calculation?
% the same edit is made with IDCG so I don't really see a benefit..
% this lowers the score in my test set, so fraud is very unlikely
% I think it lowers the score by definition, by bumping all scores back by
% one
% in effect, this treats the first and second items as equals, then the
% third as if it was the second etc

for i=1:n,
	DCG = sum(Y(i,idx(i,:)) .* denominator);
	IDCG = sum([1 1./log2(2:num_rel(i))]);
    % afaik this should be:
    %IDCG = sum([1./log2(2:num_rel(i))]);
	result = result + ((DCG / IDCG) / n);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
